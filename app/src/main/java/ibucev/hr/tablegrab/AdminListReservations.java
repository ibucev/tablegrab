package ibucev.hr.tablegrab;

import android.content.Intent;
import android.graphics.Color;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import ibucev.hr.tablegrab.Model.Event;
import ibucev.hr.tablegrab.Model.Reservation;
import ibucev.hr.tablegrab.Model.ReservationEnum;
import ibucev.hr.tablegrab.Model.User;
import ibucev.hr.tablegrab.ViewHolder.ReservationHolder;

public class AdminListReservations extends AppCompatActivity {
    private static final String TAG = "";
    private DatabaseReference databaseReference, userReference, reservationReference, clickedReservation;
    private Query query;
    private String eventId;
    private Toolbar toolbar;
    private TextView userName, dateReserved, textStatistic, textPercentage;
    private Button buttonConfirm, buttonReject;
    private AdministratorManager manager = new AdministratorManager();
    private ManageReservation manageReservation = new ManageReservation();
    private RecyclerView recyclerView;
    private FirebaseRecyclerAdapter<Reservation, ReservationHolder> firebaseRecyclerAdapter;
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_list_reservations);

        Intent intent = getIntent();
        eventId = intent.getStringExtra("EVENT_ID");
        databaseReference = FirebaseDatabase.getInstance().getReference("Event/" + eventId);
        reservationReference = FirebaseDatabase.getInstance().getReference("Reservation");

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(AdminListReservations.this));

        toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Reservations");
        toolbar.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Event event = dataSnapshot.getValue(Event.class);
                textStatistic = findViewById(R.id.textStatistic);
                textPercentage = findViewById(R.id.textPercentage);

                try {
                    int numberOfReservedTables = event.getTables() - event.getFreeTables();
                    float percentage = ((float) numberOfReservedTables / (float) event.getTables()) * 100;

                    textStatistic.setText(numberOfReservedTables + " / " + event.getTables());
                    textPercentage.setText(String.format("%.2f", percentage) + "%");
                } catch (Exception e) {
                    e.getMessage();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        query = databaseReference.child("reservations");
        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Reservation, ReservationHolder>(Reservation.class, R.layout.custom_reservation_row, ReservationHolder.class, query) {
            @Override
            protected void populateViewHolder(ReservationHolder viewHolder, final Reservation model, int position) {
                clickedReservation = getRef(position);

                setUserName(viewHolder.textName, model.getUserId());
                viewHolder.eventDate.setText(model.getDateReservation());
                viewHolder.reservationStatus.setText(model.getReservationStatus().toString());
                manageReservation.setImageWithPicasso(model.getClub().toString(), viewHolder);

                viewHolder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        createAlertDialog(model.getReservationId());
                    }
                });
            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);


//        listAdapter = new FirebaseListAdapter<Reservation>(AdminListReservations.this,
//                Reservation.class, android.R.layout.simple_list_item_2, query) {
//            @Override
//            protected void populateView(View v, final Reservation model, final int position) {
//                userName = v.findViewById(android.R.id.text1);
//                dateReserved = v.findViewById(android.R.id.text2);
//                dateReserved.setText(model.getDateReservation());
//
//                setUserName(userName, model.getUserId());
//                v.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        clickedReservation = getRef(position);
//
////                        CREATE ALERT DIALOG WITH TWO OPTIONS:
////                        CONFIRM AND REJECT
//                        createAlertDialog(model.getReservationId());
//                    }
//                });
//            }
//        };
//        listView.setAdapter(listAdapter);
    }

    public void createAlertDialog(final String reservationId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdminListReservations.this);
        View viewDialog = getLayoutInflater().inflate(R.layout.dialog_two_buttons, null);
        builder.setView(viewDialog);

        final AlertDialog dialog = builder.create();
        dialog.show();

        buttonConfirm = viewDialog.findViewById(R.id.buttonFirst);
        buttonConfirm.setText("Confirm");
        buttonReject = viewDialog.findViewById(R.id.buttonSecond);
        buttonReject.setText("Reject");

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.changeReservationStatus(clickedReservation, reservationId, "CONFIRM");
                dialog.dismiss();
            }
        });

        buttonReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.changeReservationStatus(clickedReservation, reservationId, "REJECT");
                dialog.dismiss();
            }
        });
    }

    private void setUserName(final TextView userName, final String userId) {
        userReference = FirebaseDatabase.getInstance().getReference("Users/" + userId);
        userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                userName.setText(user.getFirstName() + " " + user.getLastName());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage());
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(AdminListReservations.this, AdminEvents.class));
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.admin_event_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.deleteEvent) {
            databaseReference.removeValue();

            Query query = reservationReference.orderByChild("eventId").equalTo(eventId);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot eventId: dataSnapshot.getChildren()) {
                        eventId.getRef().removeValue();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            startActivity(new Intent(AdminListReservations.this, AdminEvents.class));
            finish();
        }

        else if (item.getItemId() == R.id.editEvent) {
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Event event = dataSnapshot.getValue(Event.class);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        else if (item.getItemId() == R.id.confirmAll) {
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot reservation: dataSnapshot.child("reservations").getChildren()) {
                        manager.changeReservationStatus(reservation.getRef(), reservation.getKey().toString(), "CONFIRM");
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        else if (item.getItemId() == R.id.rejectAll) {
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "onDataChange: TUTE SAM");
                    for (DataSnapshot reservation: dataSnapshot.child("reservations").getChildren()) {
                        manager.changeReservationStatus(reservation.getRef(), reservation.getKey().toString(), "REJECT");
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        return super.onOptionsItemSelected(item);
    }
}
