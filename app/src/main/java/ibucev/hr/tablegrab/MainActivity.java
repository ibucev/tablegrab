package ibucev.hr.tablegrab;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import ibucev.hr.tablegrab.Fragments.ClubsFragment;
import ibucev.hr.tablegrab.Fragments.HomeFragment;
import ibucev.hr.tablegrab.Fragments.ProfileFragment;
import ibucev.hr.tablegrab.Model.Token;
import ibucev.hr.tablegrab.Model.User;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    private FrameLayout frameLayout;
    private ManageReservation manager = new ManageReservation();

    private HomeFragment homeFragment;
    private ClubsFragment clubsFragment;
    private ProfileFragment profileFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = findViewById(R.id.bottomNavigation);
        frameLayout = findViewById(R.id.frameLayout);

        homeFragment = new HomeFragment();
        clubsFragment = new ClubsFragment();
        profileFragment = new ProfileFragment();

        setFragment(homeFragment);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.homeNav:
                        setFragment(homeFragment);
                        return true;
                    case R.id.clubsNav:
                        setFragment(clubsFragment);
                        return true;
                    case R.id.profileNav:
                        setFragment(profileFragment);
                        return true;
                    default:
                        return false;
                }
            }
        });


        updateToken(FirebaseInstanceId.getInstance().getToken());
    }

    private void updateToken(String token) {
        DatabaseReference tokenReference = FirebaseDatabase.getInstance().getReference("Tokens");
        Token newToken = new Token(token);
        tokenReference.child(manager.getCurrentUser().getUid()).setValue(newToken);
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit();
    }


}
