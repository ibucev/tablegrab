package ibucev.hr.tablegrab.Model;

/**
 * Created by ivanb on 2/27/2018.
 */

public class Event {
    private String eventId;
    private String userId;
    private String eventName;
    private int tables;
    private String club;
    private String eventDate;
    private int freeTables;

    public Event() {
    }

    public Event(String eventId, String userId, String eventName, int tables, String club, String eventDate) {
        this.eventId = eventId;
        this.userId = userId;
        this.eventName = eventName;
        this.tables = tables;
        this.club = club;
        this.eventDate = eventDate;
        this.freeTables = tables;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getUserId() {
        return userId;
    }

    public int getTables() {
        return tables;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setTables(int tables) {
        this.tables = tables;
    }

    public void setClubs(String club) {
        this.club = club;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public int getFreeTables() {
        return freeTables;
    }

    public void setFreeTables(int freeTables) {
        this.freeTables = freeTables;
    }
}
