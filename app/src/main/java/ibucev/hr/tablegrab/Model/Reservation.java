package ibucev.hr.tablegrab.Model;

/**
 * Created by ivanb on 3/3/2018.
 */

public class Reservation {
    private String reservationId;
    private String eventId;
    private String userId;
    private String club;
    private String dateReservation;
    private ReservationEnum reservationStatus;
    private String email;

    public Reservation() {
    }

    public Reservation(String reservationId, String eventId, String userId, String club, String dateReservation, ReservationEnum reservationStatus, String email) {
        this.reservationId = reservationId;
        this.eventId = eventId;
        this.userId = userId;
        this.club = club;
        this.dateReservation = dateReservation;
        this.reservationStatus = reservationStatus;
        this.email = email;
    }

    public Reservation(ReservationEnum reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getDateReservation() {
        return dateReservation;
    }

    public void setDateReservation(String dateReservation) {
        this.dateReservation = dateReservation;
    }

    public ReservationEnum getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationEnum reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
