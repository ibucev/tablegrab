package ibucev.hr.tablegrab.Model;

import java.text.DateFormat;

/**
 * Created by ivanb on 2/11/2018.
 */

public class User extends Person {

    public User() {
    }

    public User(String id, String email, String firstName, String lastName, String dateCreated, boolean admin){
        super(id, email, firstName, lastName, dateCreated, admin);
    }


    public User(String id, String email, String firstName, String lastName, String dateCreated, String phone) {
        super(id, email, firstName, lastName, dateCreated, phone);
    }



}
