package ibucev.hr.tablegrab.Model;

/**
 * Created by ivanb on 2/12/2018.
 */

public class Club {

    private String name;
    private String address;
    private String image;
    private String location;


    public Club() {
    }

    public Club(String name, String address, String image, String location) {
        this.name = name;
        this.address = address;
        this.image = image;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
