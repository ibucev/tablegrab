package ibucev.hr.tablegrab.Model;

/**
 * Created by ivanb on 4/3/2018.
 */

public enum ReservationEnum {
    WAITING,
    CONFIRMED,
    REJECTED
}
