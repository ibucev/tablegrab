package ibucev.hr.tablegrab.Model;

import java.text.DateFormat;

/**
 * Created by ivanb on 2/11/2018.
 */

public class Admin extends Person {
    private String clubLead;

    public Admin() {
    }
//    String id, String email, String firstName, String lastName, String dateCreated, boolean admin
    public Admin(String id, String email, String firstName, String lastName, String dateCreated, boolean admin, String clubLead) {
        super(id, email, firstName, lastName, dateCreated, admin);
        this.clubLead = clubLead;
    }

    public String getClubLead() {
        return clubLead;
    }

    public void setClubLead(String clubLead) {
        this.clubLead = clubLead;
    }
}
