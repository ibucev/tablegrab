package ibucev.hr.tablegrab.Service;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import ibucev.hr.tablegrab.ManageReservation;
import ibucev.hr.tablegrab.Model.Token;

/**
 * Created by ivanb on 4/7/2018.
 */

public class FirebaseIdService extends FirebaseInstanceIdService{
    ManageReservation manageReservation = new ManageReservation();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String tokenRefreshed = FirebaseInstanceId.getInstance().getToken();
        updateToken(tokenRefreshed);
    }

    private void updateToken(String tokenRefreshed) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Tokens");
        Token token = new Token(tokenRefreshed);
        try {
            databaseReference.child(manageReservation.getCurrentUser().getUid()).setValue(token);
        } catch (Exception e) {
            e.getMessage();
        }
    }
}
