package ibucev.hr.tablegrab;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import ibucev.hr.tablegrab.Model.Event;
import ibucev.hr.tablegrab.ViewHolder.EventHolder;

public class AdminEvents extends AppCompatActivity {

    private static final String TAG = "";
    private FirebaseRecyclerAdapter<Event, EventHolder> firebaseRecyclerAdapter;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private DatabaseReference databaseReference;
    private FirebaseAuth auth;
    private String currentUserId;
    private Query query;
    private ManageReservation manageReservation = new ManageReservation();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_events);

        databaseReference = FirebaseDatabase.getInstance().getReference("Event");
        auth = FirebaseAuth.getInstance();

        currentUserId = auth.getCurrentUser().getUid();

        recyclerView = findViewById(R.id.recyclerViewEvents);
        recyclerView.setLayoutManager(new LinearLayoutManager(AdminEvents.this));

        query = databaseReference.orderByChild("userId").equalTo(currentUserId);

        toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Events");
        toolbar.setTitleTextColor(Color.WHITE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Event, EventHolder>(Event.class,
                R.layout.custom_event_row, EventHolder.class, query) {
            @Override
            protected void populateViewHolder(EventHolder viewHolder, final Event model, int position) {
                viewHolder.eventClub.setText(manageReservation.convertEnumName(model.getClub()));
                viewHolder.eventName.setText(model.getEventName());

                viewHolder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent newIntent = new Intent(AdminEvents.this, AdminListReservations.class);
                        newIntent.putExtra("EVENT_ID", model.getEventId());
                        startActivity(newIntent);
                        finish();
                    }
                });

                manageReservation.setImageWithPicasso(model.getClub().toString(), viewHolder);
                getNumberOfReservations(model.getEventId(), String.valueOf(model.getTables()), viewHolder);
            }
        };

        recyclerView.setAdapter(firebaseRecyclerAdapter);
        query.keepSynced(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(AdminEvents.this, MainActivity.class));
        finish();
        return super.onSupportNavigateUp();
    }

    private void getNumberOfReservations(String eventId, final String tablesNumber, final EventHolder holder){
        databaseReference = FirebaseDatabase.getInstance().getReference("Event/" + eventId);
        Log.d(TAG, "getNumberOfReservations: " + databaseReference);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Event event = dataSnapshot.getValue(Event.class);
                try {
                    holder.eventStatistic.setText(event.getTables() - event.getFreeTables() + " / " + tablesNumber);
                } catch (Exception e) {
                    e.getMessage();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage() );
            }
        });
    }
}
