package ibucev.hr.tablegrab.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import ibucev.hr.tablegrab.ClubActivity;
import ibucev.hr.tablegrab.ManageReservation;
import ibucev.hr.tablegrab.Model.Club;
import ibucev.hr.tablegrab.R;


public class ClubsFragment extends Fragment {

    private DatabaseReference databaseReference, clubReference;
    private FirebaseRecyclerAdapter<Club, ClubViewHolder> firebaseRecyclerAdapter;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private ManageReservation manageReservation = new ManageReservation();

    public ClubsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_clubs, container, false);

        toolbar = view.findViewById(R.id.my_toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle("Clubs");
        toolbar.setTitleTextColor(Color.WHITE);

        recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));

        databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://tablegrab-9f396.firebaseio.com/Clubs");

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Club, ClubViewHolder>(Club.class, R.layout.card_view_club, ClubViewHolder.class, databaseReference) {
            @Override
            protected void populateViewHolder(ClubViewHolder viewHolder, final Club model, final int position) {
                clubReference = firebaseRecyclerAdapter.getRef(position);
                viewHolder.textName.setText(manageReservation.convertEnumName(model.getName()));
                viewHolder.textAddress.setText(model.getAddress());
                Picasso.with(viewHolder.imageClub.getContext()).load(model.getImage())
                        .placeholder(R.drawable.ic_placeholder).fit().into(viewHolder.imageClub);

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent clubIntent = new Intent(getActivity(), ClubActivity.class);
                        clubIntent.putExtra("CLUB_NAME", model.getName());
                        clubIntent.putExtra("CLUB_ADDRESS", model.getAddress());
                        clubIntent.putExtra("IMAGE_URL", model.getImage());
                        clubIntent.putExtra("LOCATION", model.getLocation());

                        startActivity(clubIntent);
                    }
                });
            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
        databaseReference.keepSynced(true);

        return view;
    }


}
