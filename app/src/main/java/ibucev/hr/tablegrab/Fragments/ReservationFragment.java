package ibucev.hr.tablegrab.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import ibucev.hr.tablegrab.EditReservation;
import ibucev.hr.tablegrab.ManageReservation;
import ibucev.hr.tablegrab.Model.Reservation;
import ibucev.hr.tablegrab.R;
import ibucev.hr.tablegrab.ViewHolder.ReservationHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservationFragment extends Fragment {
    private RecyclerView recyclerView;
    private FirebaseRecyclerAdapter<Reservation, ReservationHolder> firebaseRecyclerAdapter;
    private DatabaseReference databaseReference, reservationRef;
    private FirebaseAuth auth;
    private Query query;
    private String currentUser;
    private Button buttonCancel, buttonEdit;
    private ManageReservation manageReservation = new ManageReservation();


    public ReservationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_reservation, container, false);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser().getUid().toString();
        databaseReference = FirebaseDatabase.getInstance().getReference("Reservation");
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        query = databaseReference.orderByChild("userId").equalTo(currentUser);

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Reservation, ReservationHolder>(Reservation.class,
                R.layout.custom_reservation_row, ReservationHolder.class, query) {
            @Override
            protected void populateViewHolder(ReservationHolder viewHolder, final Reservation model, int position) {
                reservationRef = getRef(position);

                viewHolder.textName.setText(manageReservation.convertEnumName(model.getClub()));
                viewHolder.eventDate.setText(model.getDateReservation());
                viewHolder.reservationStatus.setText(model.getReservationStatus().toString());
                manageReservation.setImageWithPicasso(model.getClub().toString(), viewHolder);

                viewHolder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        createAlertDialog(reservationRef, model);
                    }
                });
            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
        return view;
    }

    private void createAlertDialog(final DatabaseReference myRef, final Reservation model) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getLayoutInflater().inflate(R.layout.dialog_two_buttons, null);
        builder.setView(view);

        final AlertDialog dialog = builder.create();
        dialog.show();

        buttonCancel = view.findViewById(R.id.buttonFirst);
        buttonCancel.setText("Cancel");
        buttonEdit = view.findViewById(R.id.buttonSecond);
        buttonEdit.setText("Edit");

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manageReservation.cancelReservation(myRef, model.getEventId(), model.getReservationId());
                dialog.dismiss();
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(getActivity(),EditReservation.class);
                newIntent.putExtra("CLUB_NAME", model.getClub().toString());
                newIntent.putExtra("DATE_RESERVED" , model.getDateReservation());
                newIntent.putExtra("EVENT_ID", model.getEventId());
                newIntent.putExtra("RESERVATION_ID", model.getReservationId());
                startActivity(newIntent);
                dialog.dismiss();
            }
        });
    }
}
