package ibucev.hr.tablegrab.Fragments;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ibucev.hr.tablegrab.R;

/**
 * Created by ivanb on 2/12/2018.
 */

public class ClubViewHolder extends RecyclerView.ViewHolder {
    TextView textName, textAddress;
    ImageView imageClub;
    View view;

    public ClubViewHolder(View itemView) {
        super(itemView);

        textName = itemView.findViewById(R.id.textName);
        textAddress = itemView.findViewById(R.id.textAddress);
        imageClub = itemView.findViewById(R.id.imageClub);

        view = itemView;
    }

}
