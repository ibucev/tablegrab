package ibucev.hr.tablegrab.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ibucev.hr.tablegrab.AdminEvents;
import ibucev.hr.tablegrab.CreateEvent;
import ibucev.hr.tablegrab.Model.Admin;
import ibucev.hr.tablegrab.Model.User;
import ibucev.hr.tablegrab.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserInfoFragment extends Fragment {
    private TextView name;
    private TextView email;
    private TextView dateOfRegistration;
    private DatabaseReference databaseReference;
    private FirebaseAuth auth;
    private String currentUser, userName, userEmail, userDateOfRegistration;
    private boolean isUserAdmin;
    private int numberOfReservations;
    private Button buttonAddEvent, buttonCheckEvents;
    private String clubLead;


    public UserInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_info, container, false);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("Users/" + currentUser);

        name = view.findViewById(R.id.textName);
        email = view.findViewById(R.id.textEmail);
        dateOfRegistration = view.findViewById(R.id.textDateOfRegistration);
        buttonAddEvent = view.findViewById(R.id.buttonAddEvent);
        buttonCheckEvents = view.findViewById(R.id.buttonCheckEvents);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Admin admin = dataSnapshot.getValue(Admin.class);

                userName = admin.getFirstName() + " " + admin.getLastName();
                userEmail = admin.getEmail();
                userDateOfRegistration = admin.getDateCreated();


                if(isUserAdmin = admin.isAdmin()){
                    buttonAddEvent.setVisibility(View.VISIBLE);
                    buttonCheckEvents.setVisibility(View.VISIBLE);

                    clubLead = admin.getClubLead();
                }

                name.setText(userName);
                email.setText(userEmail);
                dateOfRegistration.setText(userDateOfRegistration);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        buttonCheckEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(getActivity(), AdminEvents.class);
                startActivity(newIntent);
            }
        });

        buttonAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(getActivity(), CreateEvent.class);
                newIntent.putExtra("CLUB_LEAD", clubLead);
                startActivity(newIntent);
            }
        });

        return view;
    }

}
