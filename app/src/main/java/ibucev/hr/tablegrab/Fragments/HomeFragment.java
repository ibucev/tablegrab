package ibucev.hr.tablegrab.Fragments;


import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

import ibucev.hr.tablegrab.ManageReservation;
import ibucev.hr.tablegrab.Model.Event;
import ibucev.hr.tablegrab.Model.Reservation;
import ibucev.hr.tablegrab.Model.ReservationEnum;
import ibucev.hr.tablegrab.R;
import ibucev.hr.tablegrab.ViewHolder.Holder;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private Toolbar toolbar;
    private FirebaseRecyclerAdapter<Event, Holder> firebaseRecyclerAdapter;
    private RecyclerView recyclerView;
    private DatabaseReference databaseReference, reservationRef;
    private FirebaseAuth auth;
    private ManageReservation manageReservation = new ManageReservation();
    private ReservationEnum reservationEnum;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        databaseReference = FirebaseDatabase.getInstance().getReference("Event");
        auth = FirebaseAuth.getInstance();

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        setHasOptionsMenu(true);
        toolbar = view.findViewById(R.id.my_toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        getActivity().setTitle("Events");
        toolbar.setTitleTextColor(Color.WHITE);

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Event, Holder>(Event.class, R.layout.custom_row, Holder.class, databaseReference) {
            @Override
            protected void populateViewHolder(final Holder viewHolder, final Event model, final int position) {

                viewHolder.eventClub.setText(manageReservation.convertEnumName(model.getClub()));
                viewHolder.eventName.setText(model.getEventName());

                manageReservation.setImageWithPicasso(model.getClub().toString(), viewHolder);


                viewHolder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final DatabaseReference myRef = firebaseRecyclerAdapter.getRef(position);
                        final DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference("Event/" + model.getEventId());
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());


                        if (manageReservation.checkEventDate(model.getEventDate())) {
                            builder.setTitle("Make reservation");
                            builder.setMessage(manageReservation.convertEnumName(model.getClub()) + " - " + model.getEventDate());
                            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    reservationRef = FirebaseDatabase.getInstance().getReference("Reservation");
                                    String reservationId = myRef.push().getKey();
                                    String userId = auth.getCurrentUser().getUid().toString();
                                    String eventId = model.getEventId();
                                    String clubName = model.getClub();
                                    String email = manageReservation.getCurrentUser().getEmail();

                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                    String dateFormat = simpleDateFormat.format(new Date());

                                    if (model.getFreeTables() > 0) {
                                        Reservation reservation = new Reservation(reservationId, eventId, userId, clubName, dateFormat, reservationEnum.WAITING, email);
                                        reservationRef.child(reservationId).setValue(reservation);
                                        eventRef.child("reservations").child(reservationId).setValue(reservation);

                                        int newFreeTablesValue = model.getFreeTables() - 1;
                                        myRef.child("freeTables").setValue(newFreeTablesValue);

                                        Log.d(TAG, "Free tables: " + newFreeTablesValue);
                                        Snackbar.make(getView(), "Uspješno rezervirano: " + manageReservation.convertEnumName(clubName)
                                                + " " + model.getEventDate(), Snackbar.LENGTH_LONG).show();
                                    } else {
                                        Snackbar.make(getView(), "Neuspješna rezervacija. Nema slobodnih stolova", Snackbar.LENGTH_LONG).show();
                                    }
                                }
                            });
                            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                        } else {
                            builder.setTitle("Error");
                            builder.setMessage("We are sorry you cannot create reservation");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                        }
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });
            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
        databaseReference.keepSynced(true);
        return view;
    }
}
