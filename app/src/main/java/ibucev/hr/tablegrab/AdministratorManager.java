package ibucev.hr.tablegrab;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ibucev.hr.tablegrab.Model.Event;
import ibucev.hr.tablegrab.Model.Notification;
import ibucev.hr.tablegrab.Model.Reservation;
import ibucev.hr.tablegrab.Model.ReservationEnum;
import ibucev.hr.tablegrab.Model.Response;
import ibucev.hr.tablegrab.Model.Sender;
import ibucev.hr.tablegrab.Model.Token;
import ibucev.hr.tablegrab.Remote.APIService;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.ContentValues.TAG;

/**
 * Created by ivanb on 4/3/2018.
 */

public class AdministratorManager {
    DatabaseReference reservationReference, eventReference;
    int freeTables;

    APIService apiService;


    public void changeReservationStatus(final DatabaseReference myRef, final String reservationId, final String status) {
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                reservationReference = FirebaseDatabase.getInstance().getReference("Reservation").child(reservationId + "/reservationStatus");
                Reservation reservation = dataSnapshot.getValue(Reservation.class);
                String eventId = reservation.getEventId();
                eventReference = FirebaseDatabase.getInstance().getReference("Event/" + eventId);
                String reservationStatus = "";
                try {
                    reservationStatus = reservation.getReservationStatus().toString();
                } catch (Exception e) {
                    Log.e(TAG, "onDataChange: " + e.getMessage());
                }
                if (!reservationStatus.equals("CONFIRMED") && !reservationStatus.equals("REJECTED")) {
                    if (status == "CONFIRM"){
                        myRef.child("reservationStatus").setValue(ReservationEnum.CONFIRMED);
                        reservationReference.setValue(ReservationEnum.CONFIRMED);
                        sendNotification("CONFIRMED", reservation.getUserId());
                    } else if (status == "REJECT"){
                        Log.d(TAG, "onDataChange: " + myRef);
                        myRef.child("reservationStatus").setValue(ReservationEnum.REJECTED);
                        changeNumberOfFreeTables(eventId);
                        reservationReference.setValue(ReservationEnum.REJECTED);
                        sendNotification("REJECTED", reservation.getUserId());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage() );
            }
        });
    }

    private void sendNotification(final String reservationStatus, String userId) {
        DatabaseReference tokenReference = FirebaseDatabase.getInstance().getReference("Tokens");
        tokenReference.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Token token = dataSnapshot.getValue(Token.class);
                Notification notification = new Notification("TableGrab" , "Your reservation was " + reservationStatus.toLowerCase());
                Sender sender = new Sender(token.getToken(), notification);
                apiService = ManageReservation.getFCMService();
                apiService.sendNotification(sender).enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        if(response.body().success == 1) {
                            Log.d(TAG, "onResponse: SUCCESSFULL");
                        } else {
                            Log.d(TAG, "onResponse: UNSUCCESSFULL");
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private void changeNumberOfFreeTables(final String eventId) {
        eventReference = FirebaseDatabase.getInstance().getReference("Event").child(eventId);
        eventReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Event event = dataSnapshot.getValue(Event.class);
                int tables = event.getTables();
                int tablesNumber = 0;
                for (DataSnapshot eventChild: dataSnapshot.child("reservations").getChildren()) {
                    if (eventChild.child("reservationStatus").getValue().equals("CONFIRMED") || eventChild.child("reservationStatus").getValue().equals("WAITING")) {
                        Log.d(TAG, "eivabuc: " + eventChild.child("reservationStatus").getValue());
                        tablesNumber = tablesNumber + 1;
                    }
                }
                freeTables = tables - tablesNumber;
                eventReference.child("freeTables").setValue(freeTables);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage() );
            }
        });
    }

}
