package ibucev.hr.tablegrab.Remote;

import ibucev.hr.tablegrab.Model.Response;
import ibucev.hr.tablegrab.Model.Sender;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by ivanb on 4/7/2018.
 */

public interface APIService {
    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAA08ti_O0:APA91bEirVEjlfgjzuTycu4Jgg3aryTau3I8KkvIUV-2povz5F1Fv-96RsM4cu3JOjs30JPrJxmOCR8xisTDOmE00d4X9ec3uxJutD5QReU_XEWr8YPs62BzbNpirOE8uIDDsB7GQvx1"
    })
    @POST("fcm/send")
    Call<Response> sendNotification(@Body Sender body);
}
