package ibucev.hr.tablegrab;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ibucev.hr.tablegrab.Model.Club;
import ibucev.hr.tablegrab.Model.Event;
import ibucev.hr.tablegrab.Remote.APIService;
import ibucev.hr.tablegrab.Remote.RetrofitTableGrab;
import ibucev.hr.tablegrab.ViewHolder.EventHolder;
import ibucev.hr.tablegrab.ViewHolder.Holder;
import ibucev.hr.tablegrab.ViewHolder.ReservationHolder;

import static android.content.ContentValues.TAG;

/**
 * Created by ivanb on 3/11/2018.
 */

public class ManageReservation {
    private int numberOfFreeTables;
    private DatabaseReference eventReference, reservationReference;


    private static final String BASE_URL = "https://fcm.googleapis.com/";

    public static APIService getFCMService() {
        return RetrofitTableGrab.getClient(BASE_URL).create(APIService.class);
    }

    public void cancelReservation(final DatabaseReference myRef, final String eventId, final String reservationId){
        reservationReference = FirebaseDatabase.getInstance().getReference().child("Reservation");
        eventReference = FirebaseDatabase.getInstance().getReference().child("Event/" + eventId);
        eventReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Event event = dataSnapshot.getValue(Event.class);

                if (!dataSnapshot.child("reservations/" + reservationId).child("reservationStatus").getValue().equals("REJECTED")){
                    numberOfFreeTables = event.getFreeTables();
                    eventReference.child("freeTables").setValue(numberOfFreeTables +1);
                }
                eventReference.child("reservations").child(reservationId).removeValue();
                reservationReference.child(reservationId).removeValue();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "Error");
            }
        });
    }

    public void setImageWithPicasso(final String clubName, final Holder viewHolder) {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("Clubs/" + clubName);
        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Club club = dataSnapshot.getValue(Club.class);

                Picasso.with(viewHolder.eventImageClub.getContext()).load(club.getImage()).fit()
                        .placeholder(R.drawable.ic_placeholder).into(viewHolder.eventImageClub);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage());
            }
        });
    }

    public void setImageWithPicasso(final String clubName, final EventHolder viewHolder) {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("Clubs/" + clubName);
        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Club club = dataSnapshot.getValue(Club.class);

                Picasso.with(viewHolder.eventImageClub.getContext()).load(club.getImage()).fit()
                        .placeholder(R.drawable.ic_placeholder).into(viewHolder.eventImageClub);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage());
            }
        });
    }

    public String convertEnumName(String clubName) {
        clubName = clubName.substring(0,1) + clubName.substring(1).toLowerCase();
        return clubName;
    }

    public FirebaseUser getCurrentUser() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        return currentUser;
    }

    public void setImageWithPicasso(final String clubName, final ReservationHolder viewHolder) {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("Clubs/" + clubName);
        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Club club = dataSnapshot.getValue(Club.class);

                Picasso.with(viewHolder.clubImage.getContext()).load(club.getImage()).fit()
                        .placeholder(R.drawable.ic_placeholder).into(viewHolder.clubImage);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage());
            }
        });
    }

    
    public boolean checkEventDate(String eventDate) {
        SimpleDateFormat rightNow = new SimpleDateFormat("dd / MM / yyyy / hh");
        try {
            Date convertedDate = rightNow.parse(eventDate);
            if (new Date().before(convertedDate)) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
}
