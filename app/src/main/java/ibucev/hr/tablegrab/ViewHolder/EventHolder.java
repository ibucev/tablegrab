package ibucev.hr.tablegrab.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ibucev.hr.tablegrab.R;

/**
 * Created by ivanb on 3/21/2018.
 */

public class EventHolder extends RecyclerView.ViewHolder{

    public TextView eventClub;
    public TextView eventName;
    public ImageView eventImageClub;
    public TextView eventStatistic;

    public View view;

    public EventHolder(View itemView) {
        super(itemView);

        eventClub = itemView.findViewById(R.id.textClubName);
        eventName = itemView.findViewById(R.id.textEventName);
        eventImageClub = itemView.findViewById(R.id.eventImage);
        eventStatistic = itemView.findViewById(R.id.textEventStatistic);

        view = itemView;
    }
}
