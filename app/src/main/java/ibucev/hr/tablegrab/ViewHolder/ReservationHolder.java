package ibucev.hr.tablegrab.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ibucev.hr.tablegrab.R;

/**
 * Created by ivanb on 4/9/2018.
 */

public class ReservationHolder extends RecyclerView.ViewHolder {
    public TextView textName;
    public TextView eventDate;
    public TextView reservationStatus;
    public ImageView clubImage;

    public View view;

    public ReservationHolder(View itemView) {
        super(itemView);

        textName = itemView.findViewById(R.id.textName);
        eventDate = itemView.findViewById(R.id.textDateEvent);
        reservationStatus = itemView.findViewById(R.id.textReservationStatus);
        clubImage = itemView.findViewById(R.id.clubImage);

        view = itemView;
    }
}
