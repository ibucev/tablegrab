package ibucev.hr.tablegrab.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ibucev.hr.tablegrab.R;

/**
 * Created by ivanb on 1/29/2018.
 */

public class Holder extends RecyclerView.ViewHolder{

    public TextView eventClub;
    public TextView eventName, textReserve;
    public ImageView eventImageClub;

    public View view;

    public Holder(View itemView) {
        super(itemView);

        eventClub = itemView.findViewById(R.id.textClubName);
        eventName = itemView.findViewById(R.id.textEventName);
        eventImageClub = itemView.findViewById(R.id.eventImage);
        textReserve = itemView.findViewById(R.id.textReserve);

        view = itemView;
    }
}
