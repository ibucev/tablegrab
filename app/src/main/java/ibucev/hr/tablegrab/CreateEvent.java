package ibucev.hr.tablegrab;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

import ibucev.hr.tablegrab.Fragments.ProfileFragment;
import ibucev.hr.tablegrab.Model.ClubsEnum;
import ibucev.hr.tablegrab.Model.Event;

public class CreateEvent extends AppCompatActivity {

    private FirebaseAuth auth;
    private DatabaseReference databaseReference, myRef;

    private EditText editTables, editTitle;
    private TextView textEventDate;
    private Button buttonAddEvent, buttonPickDate;
    private Toolbar toolbar;

    private int numberOfTables;
    private String eventName, eventDate, clubLead, eventDateWithHour;
    private ManageReservation manageReservation = new ManageReservation();

    private Calendar calendar = Calendar.getInstance();
    int day, month, year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        Intent intent = getIntent();
        clubLead = intent.getStringExtra("CLUB_LEAD");

        editTables = findViewById(R.id.editTables);
        editTitle = findViewById(R.id.editTitle);

        toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create Event");
        toolbar.setTitleTextColor(Color.WHITE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        databaseReference = FirebaseDatabase.getInstance().getReference("Event");
        myRef = FirebaseDatabase.getInstance().getReference("Clubs");
        auth = FirebaseAuth.getInstance();

        textEventDate = findViewById(R.id.textEventDate);
        buttonAddEvent = findViewById(R.id.buttonAddEvent);
        buttonPickDate = findViewById(R.id.buttonPickDate);

        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);

        buttonPickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateEvent.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month = month +1;
                        eventDate = day + " / " + month + " / " + year;
                        eventDateWithHour = eventDate + " / 22";
                        textEventDate.setText(eventDate);
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });



        buttonAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(manageReservation.checkEventDate(eventDateWithHour)) {
                    String eventId = databaseReference.push().getKey();
                    String userId = auth.getCurrentUser().getUid();
                    numberOfTables = Integer.parseInt(editTables.getText().toString().trim());
                    eventName = editTitle.getText().toString().trim();

                    Event event = new Event(eventId, userId, eventName, numberOfTables, clubLead, eventDateWithHour);
                    databaseReference.child(eventId).setValue(event);

                    startActivity(new Intent(CreateEvent.this, MainActivity.class));
                    finish();
                } else {
                    Toast.makeText(CreateEvent.this, "Check your event date", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
        return true;
    }
}
