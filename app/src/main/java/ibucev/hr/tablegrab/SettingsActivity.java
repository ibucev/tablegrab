package ibucev.hr.tablegrab;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import ibucev.hr.tablegrab.Fragments.ProfileFragment;

public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = "";
    private String email;
    private String newPassword;
    private FirebaseAuth auth;
    private Button signOut, resetPassword, setNewPassword, confirmPassword;
    private EditText password;
    private Toolbar toolbar;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        auth = FirebaseAuth.getInstance();
        email = auth.getCurrentUser().getEmail();

        toolbar = findViewById(R.id.toolbarSettings);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Settings");
        toolbar.setTitleTextColor(Color.WHITE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        signOut = findViewById(R.id.signOut);
        resetPassword = findViewById(R.id.resetPassword);
        setNewPassword = findViewById(R.id.setPassword);

        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signOutUser();
            }
        });

        resetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(getApplicationContext(), "Email sent", Toast.LENGTH_LONG).show();
                            signOutUser();
                        } else {
                            Toast.makeText(getApplicationContext(), "Error! Email is not sent", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        setNewPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                view = getLayoutInflater().inflate(R.layout.dialog_password, null);
                builder.setView(view);

                final AlertDialog dialog = builder.create();
                dialog.show();

                confirmPassword = view.findViewById(R.id.setPassword);

                password = view.findViewById(R.id.passwordFirst);
                confirmPassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        newPassword = password.getText().toString();
                        changePassword(newPassword);
                        dialog.dismiss();
                    }
                });

            }
        });
    }

    private void changePassword(String newPassword) {
        user = FirebaseAuth.getInstance().getCurrentUser();
        if(!TextUtils.isEmpty(newPassword)) {
            user.updatePassword(newPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "User password updated.", Toast.LENGTH_LONG).show();
                        signOutUser();
                    } else {
                        Toast.makeText(getApplicationContext(), "Error.", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    private void signOutUser(){
        auth.signOut();
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
        return true;
    }
}
