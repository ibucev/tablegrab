package ibucev.hr.tablegrab;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditReservation extends AppCompatActivity {

    private static final String TAG = "";
    private Intent intent;
    private String clubName, eventId, dateReserved, reservationId;
    private TextView textName, textEventName, textDateReserved;
    private Button buttonCancel;
    private ManageReservation manageReservation = new ManageReservation();
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_reservation);

        intent = getIntent();

        clubName = intent.getStringExtra("CLUB_NAME");
        eventId = intent.getStringExtra("EVENT_ID");
        dateReserved = intent.getStringExtra("DATE_RESERVED");
        reservationId = intent.getStringExtra("RESERVATION_ID");

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Reservation/" + reservationId);

        textName = findViewById(R.id.textClub);
//        textEventName = findViewById(R.id.);
        textDateReserved = findViewById(R.id.textDateReserved);
        buttonCancel = findViewById(R.id.buttonCancel);

        textName.setText(clubName);
//        textEventName.setText(textEventName);
        textDateReserved.setText(dateReserved);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manageReservation.cancelReservation(databaseReference, eventId, reservationId);
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        });




    }
}
