package ibucev.hr.tablegrab;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import ibucev.hr.tablegrab.Model.Event;

public class ClubActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = "";
    private TextView textClubName, textClubAddress;
    private ImageView imageClub;
    private Intent intent;
    private ListView listView;
    private ListAdapter listAdapter;
    private DatabaseReference databaseReference;
    private Query query;
    private String imageUrl, clubName, clubAddress, clubLocation;
    private Toolbar toolbar;
    private GoogleMap map;
    private ManageReservation manageReservation = new ManageReservation();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(googleServicesAvailability()) {
            Toast.makeText(this, "Perfect", Toast.LENGTH_LONG).show();
            setContentView(R.layout.activity_club);
            initMap();
        }

        intent = getIntent();
        databaseReference = FirebaseDatabase.getInstance().getReference("Event");

        clubName = intent.getStringExtra("CLUB_NAME");
        clubName = clubName.substring(0,1) + clubName.substring(1).toLowerCase();
        clubAddress = intent.getStringExtra("CLUB_ADDRESS");
        clubLocation = intent.getStringExtra("LOCATION");

        toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(clubName);
        toolbar.setTitleTextColor(Color.WHITE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        imageClub = findViewById(R.id.imageClubInfo);
        textClubName = findViewById(R.id.textClubName);
        textClubAddress = findViewById(R.id.textClubAddress);
        listView = findViewById(R.id.listViewClub);

        textClubName.setText(manageReservation.convertEnumName(clubName));
        textClubAddress.setText(clubAddress);
        imageUrl = intent.getStringExtra("IMAGE_URL");

        Picasso.with(getApplicationContext()).load(imageUrl)
                .placeholder(R.drawable.ic_placeholder).fit().into(imageClub);

        query = databaseReference.orderByChild("club").equalTo(intent.getStringExtra("CLUB_NAME")).limitToFirst(4);
        listAdapter = new FirebaseListAdapter<Event>(this, Event.class, android.R.layout.simple_list_item_2, query) {
            @Override
            protected void populateView(View v, Event model, int position) {
                TextView eventName = v.findViewById(android.R.id.text1);
                TextView dateOfEvent = v.findViewById(android.R.id.text2);

                eventName.setText(model.getEventName());
                dateOfEvent.setText(model.getTables()+ "");
            }
        };

        listView.setAdapter(listAdapter);
    }

    public boolean googleServicesAvailability(){
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int checkAvailability = apiAvailability.isGooglePlayServicesAvailable(ClubActivity.this);

        if(checkAvailability == ConnectionResult.SUCCESS){
            return true;
        } else if (apiAvailability.isUserResolvableError(checkAvailability)) {
            Dialog dialog = apiAvailability.getErrorDialog(this, checkAvailability, 0);
            dialog.show();
        } else {
            Toast.makeText(this, "Can't connect to google play services", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private void initMap(){
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(ClubActivity.this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
        return true;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        zoomToLocation(clubLocation);
    }

    private void zoomToLocation(String clubLocation) {
        String [] location = clubLocation.split(",");
        double lat, lng;

        lat = Double.parseDouble(String.valueOf(location[0]).trim());
        lng = Double.parseDouble(String.valueOf(location[1]).trim());

        LatLng latLng = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, 16);
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(clubName);

        map.moveCamera(update);
        map.addMarker(markerOptions);
    }
}
